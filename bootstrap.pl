#! /usr/bin/perl
# This file is part of dnstools.
# Copyright (C) 2014 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use CPAN;
use Cwd qw(getcwd);
use File::Path qw(mkpath);

sub find_subdirs {
    my $line;
    my @ret;
    
    open(my $fd, "<", "Makefile") or die "Can't open Makefile: $!";
    while (<$fd>) {
	s/#.*//;
	next if (/^\s*$/);
	if (s/\\\s*$//) {
	    $line .= $_;
	    next;
	} else {
	    $line .= $_;
	    $line =~ s/^\s+//;
	    if ($line =~ s/^SUBDIRS\s*=\s*(.*)//) {
		@ret = split /\s+/, $1;
		last;
	    }
	    $line = "";
	}
    }
    close($fd);
    return @ret;
}

####
my $incdir = "inc/ExtUtils";
my $topdir = getcwd;

my @subdirs = find_subdirs();
die "No SUBDIRS found in the Makefile; make sure you run this script from the project's top-level directory" if ($#subdirs == -1);

my $modname = "ExtUtils::AutoInstall";

my $mod = CPAN::Shell->expandany($modname) or die "Can't expand $modname";
my $file = $mod->inst_file;
my $dir;

if (defined($file)) {
    print "$modname installed at $file\n";
    if (-f $file) {
	$file =~ s#/[^/]+\.pm$##;
	if (-d $file) {
	    $dir = $file;
	} else {
	    die "Can't find $modname directory";
	}
    }
} else {
    print "Getting $modname\n";
    my $distro = $mod->get or die "Can't get distribution for $modname";
    $dir = $distro->dir;
}

$file = "$dir/$incdir/AutoInstall.pm";
$file = "$dir/AutoInstall.pm" unless (-f $file);
-f $file or die "$file not found";

chdir $topdir or die "Can't change to $topdir: $!";
foreach my $d (@subdirs) {
    print "Directory $d/$incdir\n";
    if (! -d "$d/$incdir") {
	mkpath "$d/$incdir" or die "Can't create $d/$incdir: $!"; 
    }
    chdir "$d/$incdir"
	or die "Can't change to $d/$incdir: $!";
    unlink("AutoInstall.pm");
    symlink($file, "AutoInstall.pm")
	or die "Can't create symlink in ". getcwd . ": $!";
    chdir $topdir
	or die "Can't change back to $topdir: $!"; 	
}

