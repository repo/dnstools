# -*- perl -*-
# Copyright (C) 2011, 2014 Sergey Poznyakoff <gray@gnu.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use lib 'inc';
use ExtUtils::AutoInstall (
    -core => [
	 'Getopt::Long' => 2.34,
	 'Pod::Usage' => 1.51,
	 'Pod::Man' => 2.25,
	 'DBI' , => 1.607,
	 'Net::DNS' => 0.66,
	 'Net::CIDR' => 0.14
    ]
);

# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    'NAME'              => 'dnsdbck',
    'AUTHOR'            => 'Sergey Poznyakoff <gray@gnu.org>',
    'ABSTRACT'          => 'Verifies consistency of DNS information in an SQL database.',
    'FIRST_MAKEFILE'    => 'Makefile',
    'VERSION'           => '1.01',
    'EXE_FILES'         => [ 'dnsdbck' ]
);
